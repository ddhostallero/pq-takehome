# PQ take-home project

ML Task: Given a sequence of four amino acids (AAs), find the fitness (label)
- Construct 10 new sequences with the highest predicted fitness for a subsequent round of synthesis
- Suggest 10 compounds that could improve the model (see ``Answer_to_task2.pdf``)


## Data Exploration and Intuitions
First, I did data exploration and preprocessing. Please see `DataExplorationAndPreprocessing.ipynb`
- I am given 570 samples out of the 160k possible sequences
- The distribution of the fitness labels is skewed towards 0
- There are only 12 samples with fitness > 1. Since this is too few, I expect the model to have difficulty getting an output > 1 (i.e., the model will naturally center around the mean 0.09)
- Now, I think we care more about "which sequences will be fit" rather than "what is the actual fitness value"
- Therefore, we have the following options:
    - use the label as is, and use MSE as loss function
    - log-transform the label, and the MSE as loss function
    - clip the label to 0 to 1, and use BCE as loss function (think of binary classification with soft labels, anything >= 1 is "fit enough")

## Model Design
- It is possible to generate all 160k sequences because the data is discrete. Therefore, we could use an autoencoder-type feature extractor
- This is a sequence, so I used a 1-dimensional CNN for simplicity
- The model's input is the sequence and its output are the fitness prediction (0-1) and the reconstruction of the sequence (logits)
- I want the feature extractor not to be just a "compression" model, so I will be training the autoencoder and fitness predictor simultaneously
- I used ``nn.Embedding`` instead of 1-hot encoding because some AAs might be related (as supposed to all of them being orthogonal)
- The model is trained using BCE (selected due to reasons above) for the fitness prediction and Cross Entropy for the reconstruction
- I used 5-fold CV with early stopping, and then saved all 5 models

 **Note:** I tried alternative options above (MSE, log-transform, etc.) and variants of the model without the autoencoder, but they performed worse (in terms of PCC). However, these were not rigorous comparisons due to the time limitations.

## Constructing 10 sequences with highest prediction
Since 160k sequences is not a lot, I just used brute force and predicted for all of them using the 5 models (from 5-fold CV). Here are the top 10 sequences with the highest mean prediction (excluding the given dataset):

- WHGA
- HHGA
- WHGG
- MHGA
- AHGA
- IHGA
- WSCA
- HSCA
- HHGG
- WHGV

**Note**: These are also the highest mean-minus-std prediction