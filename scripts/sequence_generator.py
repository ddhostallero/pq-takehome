from torch.utils.data import Dataset
import torch

class SequenceGenerator(Dataset):
    def __init__(self, valid_codes):
        super(SequenceGenerator, self).__init__()

        # list of valid codes from 0 to 20**4-1, exclude validation set
        self.valid_codes = valid_codes
        
    def __len__(self):
        return len(self.valid_codes)
    
    def __getitem__(self, idx):
        # translate the number to the sequence
        
        idx = self.valid_codes[idx]
        x = torch.LongTensor([0]*4)
        for i in range(4):
            x[3-i] = idx%20
            idx = idx//20
        return x