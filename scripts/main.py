import numpy as np
import pandas as pd
import torch
from model import AE_Pred_Model
from trainer import Trainer
from sequence_generator import SequenceGenerator

# for logging & hyperparam file
import sys
import json
import argparse

# Allows me to log and see console output at the same time
class Logger(object):
    def __init__(self, logfile):
        self.terminal = sys.stdout
        self.log = open(logfile, "w")
   
    def write(self, message):
        self.terminal.write(message)
        self.log.write(message)  

    def flush(self):
        # this flush method is needed for python 3 compatibility.
        # this handles the flush command by doing nothing.
        pass    

_SEED = 777
torch.manual_seed(_SEED)
np.random.seed(_SEED)

mapping = pd.Series(range(20), index=['A','R','N','D','C','Q','E','G','H','I','L','K','M','F','P','S','T','W','Y','V'])
inv_mapping = pd.Series(mapping.index, mapping.values)

def tokenize(x):
    return [mapping[i] for i in x]

def stringify(x):
    seq_string = ""
    for i in range(4):
        seq_string = inv_mapping[x%20] + seq_string
        x = x//20
    return seq_string

def main(FLAGS):

    directory = FLAGS.folder
    data = pd.read_parquet('../preprocessed_data.parquet')

    models = []
    feature_cols = [str(x) for x in range(4)]

    hyp = {
        'max_epoch': 1000,
        'lr': 1e-3,
        'bs': 32,
        'ae_bs': 512,
        'aa_size':8,
        'label': FLAGS.label
        }

    # 5-fold CV
    for i in range(5):
        val_fold = i
        train_folds = [x for x in range(5) if (x != val_fold)]

        print('=====')
        print('train_folds:', train_folds)
        print('val_fold:', val_fold)

        train_data = data.loc[data['fold'].isin(train_folds)]
        val_data = data.loc[data['fold']==val_fold]

        train_dataset = torch.utils.data.TensorDataset(
            torch.LongTensor(train_data[feature_cols].values),
            torch.FloatTensor(train_data[FLAGS.label].values).view(-1,1))

        val_data_tensor = torch.LongTensor(val_data[feature_cols].values)
        val_label_tensor = torch.FloatTensor(val_data[FLAGS.label].values).view(-1,1)

        # exclude validation set from sequence generator
        w = np.asarray([20**3, 20**2, 20, 1])
        val_idx = np.matmul(val_data[feature_cols], w).values
        train_idx = list(set(range(20**4)) - set(val_idx)) 
        seq_gen = SequenceGenerator(train_idx)

        model = AE_Pred_Model(aa_size=hyp['aa_size'])
        trainer = Trainer(model, hyp)
        model, best_epoch = trainer.fit(train_dataset, val_data_tensor, val_label_tensor, seq_gen)
        
        # save model and hyperparameters
        hyp['best_epoch'] = best_epoch
        torch.save(model.state_dict(), '%s/model_weights_valfold_%d'%(directory, val_fold))
        x = json.dumps(hyp)
        f = open("%s/model_config_valfold_%d.txt"%(directory, val_fold),"w")
        f.write(x)
        f.close()

        models.append(model)


    # predict on all then use ensemble of 5 model predictions
    all_sequences = SequenceGenerator(range(160000))
    data_loader = torch.utils.data.DataLoader(all_sequences, batch_size=2048, shuffle=False)
    all_seq_pred = np.zeros((160000, 5))

    device = trainer.device
    for i in range(5):
        model = models[i].to(device)
        model.eval()
        out_list = []

        with torch.no_grad():
            for x in data_loader:
                out_list.append(model.predict(x.to(device)))

        outs = torch.concat(out_list).cpu().view(-1).numpy()
        all_seq_pred[:,i] = outs

    all_seq_pred_mean = all_seq_pred.mean(axis=1)

    count = 0
    for idx in np.argsort(all_seq_pred_mean)[::-1]: # sort according to mean prediction
        seq_string = stringify(idx)
        if seq_string not in data['Variants'].values:
            count += 1
            print(seq_string, 'new', all_seq_pred[idx])
        else:
            print(seq_string, all_seq_pred[idx])
        
        if count == 10:
            break


if __name__ == '__main__':
    parser = argparse.ArgumentParser()

    parser.add_argument("--folder", default="../results/", help="output folder")
    parser.add_argument("--label", default="clip_fitness", help="column of the label")
    args = parser.parse_args() 

    sys.stdout = Logger(args.folder+"/log.txt")

    main(args)