import torch
import torch.nn as nn
from scipy.stats import pearsonr
from copy import deepcopy

class Trainer:
    def __init__(self, model, hyp):
        self.device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
        self.model = model
        self.hyp = hyp
        self.optimizer = torch.optim.Adam(self.model.parameters(), lr=hyp['lr'])
        self.model = self.model.to(self.device)

        # The autoencoder part reconstructs a sequence of discrete characters so it is a 4-dim classification
        self.ae_loss = nn.CrossEntropyLoss()

        # although the task is regression, the number of samples >1 are too few to bias the model towards predicting anything > 1
        # So I am going to use the clipped fitness [0, 1] and use BCE loss, instead of the traditional MSE
        self.fit_loss = nn.BCELoss()
        self.alpha = 10

    def train_step(self, train_loader, ae_train_loader):
        self.model.train()
        
        for (x1,y),x2 in zip(train_loader, ae_train_loader):
            
            x1,y,x2 = x1.to(self.device), y.to(self.device), x2.to(self.device)
            self.optimizer.zero_grad()    
            
            # predict the fitness of x1 and reconstruct x2
            y_hat, recons_logits = self.model(x1, x2)
            
            fitness_loss = self.alpha*self.fit_loss(y_hat, y)
            recons_loss = self.ae_loss(recons_logits, x2)
            
            total_loss = fitness_loss + recons_loss
            total_loss.backward()
            self.optimizer.step()
            
        return fitness_loss.item(), recons_loss.item() 
        
    def val_step(self, val_data_tensor, val_label):
        # assume: validation step is 1 batch only
        self.model.eval()
    
        with torch.no_grad():
            x = val_data_tensor.to(self.device)
            y_hat, recons_logits = self.model(x,x)
            y_hat = y_hat.cpu().detach()
            
            fitness_loss = self.alpha*self.fit_loss(y_hat, val_label).numpy()
            recons_loss = self.ae_loss(recons_logits, x).cpu().detach().numpy()

        pcc = pearsonr(val_label.view(-1).numpy(), y_hat.view(-1).numpy())[0]

        return fitness_loss, recons_loss, pcc

    def fit(self, train_dataset, val_data_tensor, val_labels, seq_gen):

        data_loader = torch.utils.data.DataLoader(train_dataset, batch_size=self.hyp['bs'], shuffle=True)
        ae_train_loader = torch.utils.data.DataLoader(seq_gen, batch_size=self.hyp['ae_bs'], shuffle=True)
        
        # initial validation performance
        val_mse, val_ce, val_pcc = self.val_step(val_data_tensor, val_labels)
        print("%d \t train_err:____\ttrain_ce:____\tval_err:%.4f\tval_ce:%.4f\tval_pcc:%.4f"%(0, val_mse, val_ce, val_pcc))
        
        best_val_loss = val_mse+val_ce + 100
        best_epoch = 0
        iter_since_best = 0
            
        for i in range(self.hyp['max_epoch']):
            # train
            train_mse, train_ce = self.train_step(data_loader, ae_train_loader)
            # validate
            val_mse, val_ce, val_pcc = self.val_step(val_data_tensor, val_labels)
            print("%d \t train_err:%.4f\ttrain_ce:%.4f\tval_err:%.4f\tval_ce:%.4f\tval_pcc:%.4f"%(i+1, train_mse, train_ce, val_mse, val_ce, val_pcc))
            
            # early stopping
            if val_mse+val_ce < best_val_loss:
                iter_since_best = 0
                best_val_loss = val_mse+val_ce
                best_epoch = i+1
                # save best state (on memory only since the training is fast enough)
                best_model_state = deepcopy(self.model.state_dict())
            else:
                iter_since_best += 1
                
            if iter_since_best == 10:
                break

        # revert to the best model state
        self.model.load_state_dict(best_model_state)
        
        return self.model, best_epoch