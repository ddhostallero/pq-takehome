import torch
import torch.nn as nn
import torch.nn.functional as F

class AE_Pred_Model(nn.Module):
    def __init__(self, aa_size):
        super(AE_Pred_Model, self).__init__()
        self.aa_size = aa_size
        
        self.aa_embedding = nn.Embedding(20, aa_size) # use embedding layer instead of 1-hot encoding

        # encoder
        self.conv1 = nn.Conv1d(in_channels=aa_size, out_channels=aa_size, kernel_size=2, stride=1, padding='valid')
        self.conv2 = nn.Conv1d(in_channels=aa_size, out_channels=aa_size, kernel_size=2, stride=1, padding='valid')

        # predictor
        self.out = nn.Linear(2*aa_size, 1)
        
        # decoder
        self.dec1 = nn.Linear(2*aa_size, 3*aa_size)
        self.dec2 = nn.Linear(3*aa_size, 4*20)
    
        
    def forward(self, x1, x2):
        pred = self.predict(x1)
        x2 = self.encode(x2)

        # decoder
        x2 = F.leaky_relu(self.dec1(x2))
        # reconstruct like a classifier
        x2 = self.dec2(x2).view(-1, 20, 4) # will use crossentropy, (batch, n_class, n_dim)
        
        return pred, x2
    
    def encode(self, x):
        x = self.aa_embedding(x)        # (batch, 4, aa_size)
        x = x.transpose(1,2)            # (batch, aa_size, 4)
        x = F.leaky_relu(self.conv1(x)) # (batch, aa_size, 3) 
        x = F.leaky_relu(self.conv2(x)) # (batch, aa_size, 2)
        x = x.view(-1, 2*self.aa_size)
        return x

    def predict(self, x):
        x = self.encode(x)
        x = self.out(x)
        return torch.sigmoid(x)